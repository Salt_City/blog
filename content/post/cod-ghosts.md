---
title: "Call of Duty: Ghosts"
date: 2023-06-21T10:03:36-05:00
layout:     post
subtitle:   "Xbox One/PS4/PC - November 5th 2013"
author:     "Brandon"
---
## Does anyone even remember this one?
Hey it's been a while since I have posted here! I blame that on the fact that I haven't really been playing
too many games that would have much of a reason to have a vending machine in it.

But here we are finally; I was cruising through some old forgettable games and came across what I would consider as the most
forgettable of all the Call of Duty games. Call of Duty: Ghosts - we've got dogs! I remember this coming out and it finally
feeling like the annual CoD selling bajillions of copies finally feeling like it was coming to an end. Granted, there
were some solid blockbusters they released after this one, and CoD Warzone is a money printing machine as far as I can tell
(yours truly even has found a lot of enjoyment out of dying within a few minutes of touching down from the plane). Things just
never seemed to hit that fever-ish, people waiting outside until midnight release, feeling that the CoD games had during the peak of the 360 years
after this one came out.

Either way, I can appreciate that they got an intern to make a fake Starbucks vending machine and seemed to just liter these things all over their
multi-player maps!

![CoD-1](/img/cod-ghosts-1.png)
![CoD-2](/img/cod-ghosts-2.png)