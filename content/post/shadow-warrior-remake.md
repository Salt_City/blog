---
title: "Shadow Warrior Remake"
date: 2023-01-10T20:04:11-05:00
layout:     post
subtitle:   "PC, PS4, XBO - September 23rd 2013"
author:     "Brandon"
---

![shadow warrior](/img/shadow-warrior.png)

Look at this one. What can I even say? I can give this one more of a pass over the Red Steel 2 textures because this is pretty much
just window dressing as you wander through the level, but that stuff is still **pretty** blurry. Nice touch on top with the refigeration
vent, but when I walk up to it and it looks like a mess of blurry cardboard cutouts I say why even bother!

Some interesting facts about Shadow Warrior Remake:
1. Not sure if you noticed, but it is a remake of a game by the same name which came out in 1997
2. The original developers also were responsible for Duke Nukem 3D
3. Two actual books were written by a person based on the source material