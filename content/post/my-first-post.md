---
title: "Freedom Fighters"
date: 2023-01-08T23:40:36-05:00
layout:     post
subtitle:   "Original Xbox - October 1st 2003"
author:     "Brandon"
---
## First Post!
This is an inaugural post for my own personal blog site that revolves around interesting things I like. One of
those things happens to be cataloging video game vending machines, and as you might guess the plan is to share pictures
of vending machines in video games that I happen to come across and write about any other topics that I feel I might
have something interesting to say about.

![Freedom Fighters](/img/freedom-fighters.png)
