---
title: "Red Steel 2"
date: 2023-01-10T00:18:28-05:00
layout:     post
subtitle:   "Wii - March 23rd 2010"
author:     "Brandon"
---
![Red Steel 2](/img/red-steel-2.png)

Now, would I take this vending machine home to my mother? No, of course not, because my mom doesn't talk to me. But I
also find the texture on this one to be especially poor, considering we are talking about 2010 here. It's a first person
shooter and these things are scattered around all over the place, so you would think that a little more effort would be
put into these little objects you slash up to collect a little bit of money.

Some fun facts about Red Steel 2:
1. Did you know it is the sequal to Red Steel?
2. Did you know Red Steel was a launch title that was complete garbage?
3. Did you know that Red Steel 2 required the Motion Plus controller?
4. Did you know that the Wii was just a gamecube with a six-axis slapped onto it?